const mysql = require('mysql');
const configConstant = require('../config/config');
const _ = require('lodash')

module.exports = {
    connect: async function(){
        return await new Promise(async (resolve)=>{
            let connection = await mysql.createConnection({
                host: configConstant.Details["Mysql.host"],
                user: configConstant.Details["Mysql.user"],
                password: configConstant.Details["Mysql.password"],
                database: configConstant.Details["Mysql.database"],
                //port: 8889
                // insecureAuth: true
              });
              resolve(connection);
        })
    },
    AddMultipleRecords: async(arg) => {
        return await new Promise(async function (resolve, reject) {
          let dbConnection = await module.exports.connect();
          let table = arg.tableName;
          let column_name = arg.column_name;
          let addData = arg.addData;
          let sql = `INSERT INTO ${table} ${column_name}  VALUES ?  `;
          await dbConnection.query(sql, [addData], async (err, result) => {
            if (err) {
              console.log("Err in insert query", err);
              // throw err;
              await dbConnection.end();
              reject(err);
            }
            else {
              console.log('Row inserted:' + result.affectedRows);
              addData['insertId'] = result.insertId;
              await dbConnection.end();
              resolve(addData);
            }
          })
        });
      },
      AddRecords: async(arg) => {
        return await new Promise(async function (resolve, reject) {
          let dbConnection = await module.exports.connect();
          let table = arg.tableName;
          let addData = arg.addData;
          let sql = `INSERT INTO ${table} SET ?`;
          await dbConnection.query(sql, addData, async (err, result) => {
            if (err) {
              console.log("Err in insert query", err);
              // throw err;
              await dbConnection.end();
              reject(err);
            }
            else {
              addData['insertId'] = result.insertId;
              await dbConnection.end();
              resolve(addData);
            }
          })
        });
      },
      GetRecords:async (arg) => {
        return await new Promise(async function (resolve, reject) {
          let dbConnection = await module.exports.connect();
          // main sql query
          let table = arg.tableName;
          let fields = (_.isEmpty(arg.fields)) ? '*' : arg.fields;
          let where = (_.isEmpty(arg.where)) ? '' : arg.where;
          let orderby = (_.isEmpty(arg.orderby)) ? '' : arg.orderby;
          let groupby = (_.isEmpty(arg.groupby)) ? '' : arg.groupby;
          let limit = (_.isEmpty(arg.limit)) ? '' : arg.limit;
          let offset = (_.isEmpty(arg.offset)) ? '' : arg.offset;
    
          let sql = '';
          if (_.isEmpty(where)){
            sql = `SELECT ${fields} FROM ${table}  ${groupby} ${orderby} ${limit} ${offset}`;
          }
           
          else{
            sql = `SELECT ${fields} FROM ${table} WHERE ${where} ${groupby} ${orderby} ${limit} ${offset}`;
          }
          let query = await dbConnection.query(sql, async(err, result) => {
            if (err) {
              // throw err;
              await dbConnection.end();
              reject(err);
            }
            else {
              await dbConnection.end();
              resolve(result);
            }
          });
        });
    
      },
      UpdateRecords: async(arg) => {
        return await new Promise(async function (resolve, reject) {
          let dbConnection = await module.exports.connect();
          let updateObject = arg.updateData;
          let table = arg.tableName;
          let where = (_.isEmpty(arg.where)) ? '' : arg.where;
          let Sql = '';
          if (_.isEmpty(where)) {
            Sql = `UPDATE ${table} SET ?`;
          }
          else {
            Sql = `UPDATE ${table} SET ?   WHERE ${where}`;
          }
          console.log(Sql)
          let query = await dbConnection.query(Sql, updateObject, async (err, result) => {
            if (err) {
              // throw err;
              console.log("Err in select query");
              await dbConnection.end();
              reject(err);
            }
            else {
              await dbConnection.end();
              resolve(result);
            }
          });
        });
      },
      DeleteRecords: async(arg) => {
        return await new Promise(async (resolve, reject) => {
          let dbConnection = await module.exports.connect();
          let table = arg.tableName;
          let where = arg.where;
          let Sql = '';
          if (_.isEmpty(where))
            Sql = `DELETE FROM ${table}`;
          else
            Sql = `DELETE FROM ${table} WHERE ${where}`;
            console.log(Sql)
            let query = await dbConnection.query(Sql, async (err, result) => {
              if (err) {
                await dbConnection.end();
                console.log("Err in select query", Sql);
                reject(err);
                // throw err;
              }
              else {
                await dbConnection.end();
                resolve(result);
              }
            });
        });
      },
      CustomQuery: async (arg) => {
        return await new Promise(async function (resolve, reject) {
          let dbConnection = await module.exports.connect();
          let sql = arg.sql;
          console.log(sql)
          await dbConnection.query(sql, async(err, result) => {
            if (err) {
              console.log("Err in select query", sql);
              await dbConnection.end();
              reject(err);
            }
            else {
              await dbConnection.end();
              resolve(result);
            }
          });
         
        })
      },
}