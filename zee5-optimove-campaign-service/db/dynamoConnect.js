const AWS = require('aws-sdk');
AWS.config.update({
     region: 'ap-south-1',
     //endpoint: "http://localhost:8000"
});
const dynamodb = new AWS.DynamoDB();
const dynamo = new AWS.DynamoDB.DocumentClient();

module.exports = {
     createNewTable: async function (params) {
          return await new Promise(async (resolve, reject) => {
               await dynamodb.createTable(params, async function (err, data) {
                    if (err) {
                         console.error("Unable to create table. Error JSON:", JSON.stringify(err, null, 2));
                         reject(err);
                    } else {
                         console.log("Created table. Table description JSON:", JSON.stringify(data, null, 2));
                         resolve(data);
                    }
               });
          }).then((resp) => {
               return resp;
          }).catch(err => {
               return err;
          })
     },

     putInDynamo: async function (params) {
          return await new Promise(async (resolve, reject) => {
               dynamo.put(params, async function (err, data) {
                    if (err) {
                         //console.error("Unable to add Error JSON:", JSON.stringify(err, null, 2));
                         reject(err);
                    } else {
                         console.log("PutItem succeeded:", data);
                         resolve(data);
                    }
               });
          }).then((resp) => {
               return resp;
          }).catch(err => {
               return err;
          })
     },

     getFromDynamo: async function (params) {
          return await new Promise(async (resolve, reject) => {
               dynamo.get(params, function (err, data) {
                    if (err) {
                         console.error("Unable to read item. Error JSON:", JSON.stringify(err, null, 2));
                         reject(err);
                    } else {
                         console.log("GetItem succeeded:", JSON.stringify(data, null, 2));
                         resolve(data);
                    }
               });
          }).then((resp) => {
               return resp;
          }).catch(err => {
               return err;
          })
     },
     update : async function (params) {
          return await new Promise(async (resolve, reject) => {
               dynamo.update(params, function (err, data) {
                    if (err) {
                         console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
                         reject(err);
                    } else {
                         console.log("update succeeded:", JSON.stringify(data, null, 2));
                         resolve(data);
                    }
               });
          }).then((resp) => {
               return resp;
          }).catch(err => {
               return err;
          })
     },

}