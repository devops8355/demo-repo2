const Redis = require('ioredis');

module.exports = {

  client: async function () {
    return await new Promise(async (resolve, reject) => {
      try {
        let connect = new Redis({
          port: 6379, // Redis port
          host: "localhost", // Redis host
          family: 4, // 4 (IPv4) or 6 (IPv6)
          password: "",
          db: 0,
        });
        if(connect.status != "ready"){
          setTimeout(async()=>{
            resolve(connect)
          },500)
          //reject(connect)
        }else{
          resolve(connect)
        }
      } catch (err) {
        reject(err);
      }
    }).then(resp => {
      console.log('connected to redis successfully');
      return resp;
    }).catch(err => {
      console.log(err);
      return err;
    })
  },

  getRedisObj: async function (connect,key) {
    return await new Promise(async (resolve, reject) => {
      if (connect.status == 'ready') {
        connect.get(key, async (err, data) => {
          if (err) {
            console.log(err);
            reject(err)
          } else {
            //console.log(data);
            resolve(data)
          }
        });
      } 
      else {
        console.log("connection was not established",connect, connect.status);
        resolve(0)
      }
    }).then(resp => {
      return resp;
    }).catch(err => {
      console.log(err);
      return err;
    })
  },

  setRedisObj: async function (connect,key, value) {
    return await new Promise(async (resolve, reject) => {
      if (connect.status == 'ready') {
        connect.set(key, value, async (err, data) => {
          if (err) {
            console.log(err);
            reject(err)
          } else {
            // console.log(data);
            resolve(data)
          }
        });
      } else {
        console.log("connection was not established");
        resolve(0)
      }
    }).then(resp => {
      return resp;
    }).catch(err => {
      console.log(err);
      return err;
    })
  },

  setRedisHset: async function (connect,parent, key, value) {
    return await new Promise(async (resolve, reject) => {
      if (connect.status == 'ready') {
        connect.hset(parent, key, value, async (err, data) => {
          if (err) {
            console.log(err);
            reject(err)
          } else {
            // console.log(data);
            resolve(data)
          }
        });
      } 
      // else {
      //   console.log("connection was not established");
      //   resolve(0)
      // }
    }).then(resp => {
      return resp;
    }).catch(err => {
      console.log(err);
      return err;
    })
  },

  getRedisHset: async function (connect,parent, key) {
    return await new Promise(async (resolve, reject) => {
      if (connect.status == 'ready') {
        connect.hget(parent, key, async (err, data) => {
          if (err) {
            console.log(err);
            reject(err)
          } else {
            // console.log(data);
            resolve(data)
          }
        });
      }
      //  else {
      //   console.log("connection was not established");
      //   resolve(0)
      // }
    }).then(resp => {
      return resp;
    }).catch(err => {
      console.log(err);
      return err;
    })
  },

}