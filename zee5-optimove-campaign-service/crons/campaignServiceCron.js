const moment = require('moment');
const dynamoModels = require('../db/dynamoConnect');
const redisConnect = require('../db/redisConnect');
const mysqlConnect = require("../db/mysqlConnect");
const cron = require('node-cron');
const _helper = require('../helper/helper')
const controllers = require('../controllers/controllers')
const fs = require('fs');


cron.schedule("* * * * *", async () => {
  console.log("Running Campaign Service Cron");
  try {
    if (!fs.existsSync(`CronCamapignService.json`)) {
      console.log("<--Start-->", moment().format("YYYY-MM-DD HH:mm:ss"));
      console.log("No existing Campaign Service Cron is running, looking for new one.");

      await fs.writeFileSync(`CronCamapignService.json`, `busy`);
      let resp = [], resultResp = [];
      let mysqlConnection = await mysqlConnect.connect();
      let cDate = moment().format("YYYY-MM-DD");
      let getQueryArguments = {
        tableName: "campaign_details",
        where: `DATE_FORMAT(scheduled_time,"%Y-%m-%d") = '${cDate}' AND type = 'social' AND status = 'pending' AND tags LIKE "%CS_%"`,
        fields:`campaign_id, targetgroupid, tags, type, scheduled_time, campaign_source,status,template_id, action_id`
      };
      let fetchResult = await mysqlConnect.GetRecords(getQueryArguments);
      if (fetchResult.length > 0) {
        for (let i = 0; i < fetchResult.length; i++) {
          resp = await controllers.saveCampaignServiceUsers(fetchResult[i], mysqlConnection);
        }

        resultResp["message"] = "Records Fetched";
        resultResp["data"] = resp;
      }
      await fs.unlink(`CronCamapignService.json`, function (err) {
        if (err) {
          console.log("Campaign_Service_Cron_Error", err)
        }
        /*if (err) throw err*/
      });
      //await mysqlConnection.end();
      console.log("end", moment().format("YYYY-MM-DD HH:mm:ss"));
    }
  } catch (e) {
    console.log("Optimove_Campaign Service_Error", e)
    if (fs.existsSync(`CronCamapignService.json`)) {
      fs.unlink(`CronCamapignService.json`, function (err) {
        if (err) {
          console.log("Optimove_Campaign Service_Error", err)
        }
        /*if (err) throw err*/
      });
    }

  }
})
