const _helper = require('../helper/helper');
const moment = require('moment');
const dynamoModels = require('../db/dynamoConnect');
const configConstant = require('../config/config');
const optimoveService = require('../services/OptimoveService');
const mysqlConnect    = require('../db/mysqlConnect');
const redisConnect    = require('../db/redisConnect');

/*
Engagement API requirements:  USERID, engagement Data, campaign_id
*/

module.exports = {
    saveCampaignServiceUsers: async (payload, connection) => {
        // console.log("reached")
        let updated_date = moment().format("YYYY-MM-DD HH:MM");
        let redisConnection = await redisConnect.client();
        let updateQueryArguments = {
            tableName: "campaign_details",
            where: `campaign_id = '${payload.campaign_id}' AND type = 'social' AND status = 'pending'`,
            updateData: {
                status: 'processing',
                updated_date: updated_date,
                campaign_fetch_startime: updated_date
            }
        };
        let updateResult = await mysqlConnect.UpdateRecords(updateQueryArguments);
        //   console.log(updateResult)
        let tag = payload.tags,name,campaign;
        if (tag.indexOf("IN-APP") != -1) {
            //campaign_names.push("IN-APP");
            campaign = {
                "CampaignId": 1,
                "CampaignName": "IN-APP",
                "Impression": 0,
                "Engagement": {}
            }
            name = "IN-APP";
        } else if (tag.indexOf("HIGH-LOW") != -1) {
            campaign = {
                "CampaignId": 2,
                "CampaignName": "HIGH-LOW",
                "Impression": 0,
                "Engagement": {}
            }
            name = "HIGH-LOW";
        }
        // console.log(name, campaign)
        let changed = (updateResult) ? updateResult.affectedRows : 0;
        if (changed == 1) {
            return await new Promise(async (resolve, reject) => {
                //configConstant.Details.OptimoveLoopingCount
                for (let i = 0; i < configConstant.Details.OptimoveLoopingCount; i++) {
                    let reqPayload = {
                        'CampaignID': payload.campaign_id,
                        'limit': 1000,
                        'channel': '509', //payload.ChannelID,
                        'skip': i
                    }
                    let userList = await optimoveService.getUserList(reqPayload);
                    if (userList.status != 200) {
                        reject({
                            "message": 'error encountered while fetching data',
                            "err": JSON.stringify(userList)
                        });
                    } else {
                        if (userList.data && userList.data.length == 0) {
                            console.log("=========no new data==========")
                            let updateQueryArguments = {
                                tableName: "campaign_details",
                                where: `campaign_id = '${payload.campaign_id}' AND type = 'social' AND status = 'processing'`,
                                updateData: {
                                    status: 'saved',
                                    updated_date: moment().format("YYYY-MM-DD HH:MM"),
                                    campaign_fetch_endtime: moment().format("YYYY-MM-DD HH:MM")
                                }
                            };
                            let updateResult = await mysqlConnect.UpdateRecords(updateQueryArguments);
                            resolve({
                                'message': "Users Successfully added into dynamo DB",
                                'response': updateResult
                            })
                            console.log("message")
                            break;
                        } else if (userList.data.length > 0) {
                            console.log("=========new data==========")
                            //write a function that does an upsert along with formatting the user object to send to dynamo
                            userList.data.forEach(async (userItem) => {
                                let date = moment().format("YYYY-MM-DD HH:mm:ss");
                                let record = {
                                    'UserId': userItem.CustomerID,
                                    'CampaignNames': [name],
                                    'Campaigns': [campaign],
                                    "LastUpdateAt": date,
                                    "CreatedAT": date,
                                    "Zee5Platform": "Web",
                                    "Zee5Version": "2.0"
                                };
                                let resp = await _helper.saveRecordsInDynamo(record, 'insert')
                                if (resp.code == "ConditionalCheckFailedException") {
                                    let val = await redisConnect.getRedisObj(redisConnection,userItem.CustomerID);
                                    if ((val) && val.indexOf(name) == -1) {
                                    console.log("=========================", val, val.indexOf(name))
                                        let userObj = JSON.parse(val)
                                        let oldCampaign = userObj.Campaigns;
                                        if(typeof userObj.Campaigns == 'string'){
                                            oldCampaign = JSON.parse(userObj.Campaigns)
                                        }
                                        
                                        oldCampaign.push(campaign)
                                        console.log("==============", oldCampaign, campaign, "==============")
                                        let campaign_names = userObj.CampaignNames;
                                        campaign_names.push(name);
                                        let record = {
                                            Key: {
                                                'UserId': `${userItem.CustomerID}`
                                            },
                                            UpdateExpression: "set #Campaigns = :tag, #CampaignName = :campaign_name, #LastUpdateAt = :date",
                                            ExpressionAttributeNames: {
                                                '#Campaigns': 'Campaigns',
                                                '#CampaignName': 'CampaignNames',
                                                '#LastUpdateAt': 'LastUpdateAt'
                                            },
                                            ExpressionAttributeValues: {
                                                ':tag': oldCampaign,
                                                ':campaign_name': campaign_names,
                                                ':date': `${date}`
                                            },
                                            ReturnValues: "UPDATED_NEW"
                                        };
                                        let updateRecInDynamo = await _helper.saveRecordsInDynamo(record, 'update')
                                        if (!updateRecInDynamo.code) {
                                            let fetchParams = {
                                                TableName: configConstant.Details.dynamoTableName,
                                                Key: {
                                                    "UserId": userItem.CustomerID
                                                }
                                            };
                                            let fetch = await dynamoModels.getFromDynamo(fetchParams);
                                            let setRedis = await redisConnect.setRedisObj(redisConnection,userItem.CustomerID, JSON.stringify(fetch.Item))
                                        } else {
                                            console.log("error encountered", updateRecInDynamo)
                                        }
                                    } else {
                                        //console.log(name)
                                        //console.log("pre-existing campaign")
                                    }
                                } else if (resp.code) {
                                    let val = await redisConnect.getRedisObj(redisConnection,userItem.CustomerID);
                                    console.log(resp, val)
                                } else {
                                    let setRedis = await redisConnect.setRedisObj(redisConnection,userItem.CustomerID, JSON.stringify(record))
                                    //    console.log(resp,setRedis)
                                }
                            });
                        } else {
                            reject({
                                "message": 'could not find user data from optimove',
                                "err": JSON.stringify(userList)
                            });
                        }
                    }
                }
            }).then(async (resp) => {
                await connection.end();
                return resp;
            }).catch(async err => {
                await connection.end();
                return err;
            })
        } else {
            await connection.end();
            return "no relevant record found"
        }
    },
    engagementController : async (engagementPayload) =>{
        return new Promise(async (resolve,reject)=>{
            if(engagementPayload.CampaignId && engagementPayload.UserId && engagementPayload.EngagementObj){
                let redisConnection = await redisConnect.client();
                    // let getItemFromRedis = await redisConnect.getRedisObj(engagementPayload.UserId);
                    let fetchParams = {
                        TableName: configConstant.Details.dynamoTableName,
                        Key: {
                            "UserId": engagementPayload.UserId
                        }
                    };
                    let getItemFromDynamo = await dynamoModels.getFromDynamo(fetchParams);
                    if((getItemFromDynamo) || getItemFromDynamo != undefined || getItemFromDynamo != '' ){
                        let campaignData;
                        console.log(typeof getItemFromDynamo)
                        if(getItemFromDynamo.Campaigns == undefined || getItemFromDynamo.Campaigns == 'undefined'){
                            campaignData = getItemFromDynamo['Item'];
                        }else{
                            campaignData = getItemFromDynamo;
                        }
                       console.log(campaignData.Campaigns[0].CampaignId)
                        for(let i = 0; i < campaignData.Campaigns.length; i++){
                            if(engagementPayload.CampaignId == campaignData.Campaigns[i].CampaignId ){
                                let date = moment().format("YYYY-MM-DD HH:MM:SS");
                                let newEngagementObj; 
                                if(engagementPayload.EngagementObj.UserCancel == true || engagementPayload.EngagementObj.UserCancel.toUpperCase() == 'TRUE'){
                                    newEngagementObj  = {
                                    "UserCancel": "TRUE", 
                                    "InAppRating":campaignData.Campaigns[i].Engagement.InAppRating,
                                    "EngagedAt":date
                                  }
                                }else{
                                    newEngagementObj = engagementPayload.EngagementObj;
                                }
                              
                                let impression = parseInt(campaignData.Campaigns[i].Impression)+1;
                                let params = {
                                    TableName:configConstant.Details.dynamoTableName,
                                    Key: {'UserId': `${engagementPayload.UserId}`},
                                    UpdateExpression: `set #Campaigns[${i}].Engagement=:Engagement,#Campaigns[${i}].Impression=:impression,#LastUpdateAt=:date`,
                                    ExpressionAttributeNames:{
                                        '#Campaigns': `Campaigns`,
                                        '#LastUpdateAt':'LastUpdateAt',
                                    },
                                    ExpressionAttributeValues:{
                                        ':Engagement':newEngagementObj,
                                        ':impression':impression,
                                        ':date':`${date}`

                                    },
                                    ReturnValues:"UPDATED_NEW"
                                };

                                let updateItemInToDynamo = await dynamoModels.update(params);
                                if(updateItemInToDynamo){
                                    let fetchParams = {
                                        TableName: configConstant.Details.dynamoTableName,
                                        Key: {
                                            "UserId": engagementPayload.UserId
                                        }
                                    };
                                    let fetch = await dynamoModels.getFromDynamo(fetchParams);
                                    let setRedis = await redisConnect.setRedisObj(redisConnection,engagementPayload.UserId,JSON.stringify(fetch.Item))
                                    resolve({status:200,message:'Engagement Updated.', "data":fetch.Item});
                                }else{
                                    reject({status:500,message:'Could not update the engagement object'})
                                }
                            }
                        }
                    }
            }else{
                reject({status:400,message:'UserId OR campaignId OR engagement Obj missing.'})
            }
        }).then(resp =>{
            return resp;
        }).catch(err =>{
            return err;
        })
    },
    getUserCampaign : async (userId) =>{
        return await new Promise(async(resolve, reject)=>{
            let redisConnection = await redisConnect.client();
           let userObj;
           let obj =  await redisConnect.getRedisObj(redisConnection,userId);
           if(!obj){
            let fetchParams = {
                TableName: configConstant.Details.dynamoTableName,
                Key: {
                    "UserId": userId
                }
            };
            userObj = await dynamoModels.getFromDynamo(fetchParams)
            userObj = userObj.Item;
           }else{
            userObj = JSON.parse(obj);
           }
           if(userObj.Campaigns){
            userObj.Campaigns.forEach(async(Campaign)=>{
                if(Campaign.CampaignId == 1){
                    if(Campaign.Engagement){
                        if(typeof Campaign.Engagement == 'string'){
                            engagement = JSON.parse(Campaign.Engagement);
                        }else{
                            engagement = Campaign.Engagement
                        }
                        
                    if((engagement.InAppRating<1) ||(!engagement.InAppRating)){
                        resolve({"status":200,"data":userObj})
                    }else{
                        resolve({"status":200,"data":{},"message":"User has already given Rating"})
                    }
                }else{
                    resolve({"status":200,"data":userObj});
                }
                }else if(Campaign.CampaignId == 2){
                    if(Campaign.Impression<10){
                        resolve({"status":200,"data":userObj})
                    }else{
                        resolve({"status":200,"data":{},"message":"User has already received the High-Low Campaign"})
                    }
                }else{
                    //new addition
                }
            })
           }else{
               console.log(userObj)
            resolve({"status":200,"data":{},"message":"User not eligible for the Campaign"})
           }
        }).then((resp)=>{
            return resp;
        }).catch(err=>{
            return err;
        })
    },
    setUserImpression : async (impression) =>{
        return await new Promise(async(resolve, reject)=>{
            let redisConnection = await redisConnect.client();
            let fetchParams = {
                TableName: configConstant.Details.dynamoTableName,
                Key: {
                    "UserId": impression.UserId
                }
            };
            let userObj = await dynamoModels.getFromDynamo(fetchParams);
            let date = moment().format("YYYY-MM-DD HH:MM:SS");
            userObj.Item.Campaigns.forEach(async(Campaign, index)=>{
                if(Campaign.CampaignId == impression.CampaignId && impression.Impression.toUpperCase() =="TRUE"){
                    let impressionVal = parseInt(Campaign.Impression)+1;
                    let params = {
                        TableName:configConstant.Details.dynamoTableName,
                        Key: {'UserId': `${impression.UserId}`},
                        UpdateExpression:`set #Campaigns[${index}].Impression=:impression,#LastUpdateAt=:date`,
                        ExpressionAttributeNames:{
                            '#Campaigns': `Campaigns`,
                            '#LastUpdateAt':'LastUpdateAt',
                        },
                        ExpressionAttributeValues:{
                            ':impression':impressionVal,
                            ':date':`${date}`

                        },
                        ReturnValues:"UPDATED_NEW"
                    };
                    let updateItemInToDynamo = await dynamoModels.update(params);
                    if(updateItemInToDynamo){
                            let fetchParams = {
                                TableName: configConstant.Details.dynamoTableName,
                                Key: {
                                    "UserId": impression.UserId
                                }
                            };
                            let fetch = await dynamoModels.getFromDynamo(fetchParams);
                            let setRedis = await redisConnect.setRedisObj(redisConnection,impression.UserId,JSON.stringify(fetch.Item));
                            resolve({"data":fetch.Item, "status":200})
                    }else{
                        reject({"data":{}, "status":500,"message":"update impression not successful"})
                    }
                }
            })
        }).then(async (resp)=>{
            return resp
        }).catch(err =>{
            return err;
        })
    }
}
