/*
  Modified by : Prathmesh 
  Date : 10-01-2020
*/
"use strict";
const express        = require('express'); 
const app            = express();
const port           = process.env.PORT || 3000;
const path           = require('path');
const bodyParser     = require('body-parser');
const configConstant = require('./config/config');
const engagementRoute = require('./routes/engagementRoute')

require('./crons/campaignServiceCron.js');


// Add headers
app.use((req, res, next) => {
  if (req.method === 'OPTIONS') 
  {
    var headers = {};
    headers["Access-Control-Allow-Origin"] = req.headers.origin;
    headers["Access-Control-Allow-Origin"] = "*";
    headers["Access-Control-Allow-Methods"] = "POST, GET, PUT, DELETE, OPTIONS";
    headers["Access-Control-Allow-Credentials"] = false;
    headers["Access-Control-Max-Age"] = '86400'; // 24 hours
    headers["Access-Control-Allow-Headers"] = "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept";
    res.writeHead(200, headers);
    res.end();
  }
  else
  {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.setHeader('Access-Control-Allow-Credentials', true);
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));   
    app.use('/campaignService',engagementRoute);
    next();
  }
});

app.listen(port,'0.0.0.0', (err)=>
{
  if (err) throw err;
  console.log(`Running RestHub on port ${port}`);
});

