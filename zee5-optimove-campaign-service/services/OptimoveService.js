const _helper = require('../helper/helper');
const configConstant = require('../config/config');
const Get_Customer_Exrcution_details_Api_Url = `https://api4.optimove.net/current/customers/GetCustomerExecutionDetailsByCampaign`;

module.exports = {
    getUserList : async function (payload) {
        let AuthToken = await _helper.getVerifiedToken(payload.source);
        let CampaignID = payload.CampaignID;
        let top = payload.limit;
        let channel = payload.channel;
        let page = payload.skip;
        // let  urls = `${Get_Customer_Exrcution_details_Api_Url}?CampaignID=${CampaignID}&CustomerAttributes=Email;Country&IncludeControlGroup=True`;
        let urls = `${Get_Customer_Exrcution_details_Api_Url}?CampaignID=${CampaignID}&ChannelID=${channel}&CustomerAttributes=Email;Mobile;FirstName&$top=${top}`;
        if (page > 0) {
            // urls = `${Get_Customer_Exrcution_details_Api_Url}?CampaignID=${CampaignID}&CustomerAttributes=Email;Country&$skip=${page}&IncludeControlGroup=True`;
            urls = `${Get_Customer_Exrcution_details_Api_Url}?CampaignID=${CampaignID}&ChannelID=${channel}&CustomerAttributes=Email;Mobile;FirstName&$top=${top}&$skip=${page}`;
        }
        console.log(urls)
        let option = {
            url: urls,
            method: 'GET',
            headers: {
                'Authorization-Token': AuthToken
            },
            responseType: 'json',
        }
        console.log('helperResponse', option)
        return await new Promise(async (resolve) => {
            let res = await _helper.GetMethod(option);
            resolve(res);
        })
    }
}

