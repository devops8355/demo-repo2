const configConstant  = require('../config/config')
const axios           = require('axios');
const _fs             = require('fs');
const _               = require('lodash');
const moment          = require('moment');
const readline        = require('readline');
const dynamoModels    = require('../db/dynamoConnect');
const Get_Customer_Exrcution_details_Api_Url = `https://${configConstant.Details._OptimoveDetails._Api_Url}/current/customers/GetCustomerExecutionDetailsByCampaign`;
let urls = `${Get_Customer_Exrcution_details_Api_Url}?CampaignID=7694&CustomerAttributes=Email;&$top=1`;


module.exports ={
    ResponseAndError: async (data) => {
        return {
            'status': ((data && data.status) ? data.status : ''),
            'message': ((data && data.message) ? data.message : ''),
            'line': ((data && data.line) ? data.line : ''),
            'data': ((data && data.data) ? data.data : '')
        }
    },
    GetMethod: async (option) => {
        try {
            return await new Promise(async (resolve, reject) => {
                await axios(option).then(function (response) {
                    resolve(response);
                }).catch(function (error) {
                    reject(error)
                });
            })
        } catch (error) {
            return error;
        }
    },
    PostMethod: async (option) => {
        try {
            return await new Promise(async (resolve, reject) => {
                await axios(option).then(function (response) {
                    resolve(response);
                }).catch(function (error) {
                    reject(error);
                });
            });
        } catch (error) {
            return error;
        }
    },
    tokenVerification: async (token) => {
        //new function to check the validity of the auth token
        let options = {
            url: urls,
            method: 'GET',
            headers: { 'Authorization-Token': token },//removing token could generate an error, when any of the endpoint gets called
            responseType: 'json',
        };
        // console.log(urls)
        let verifyTokenApi = await module.exports.GetMethod(options).catch(async (err) => {
            console.log("Token_ERROR->", err);
            return err;
        });
        if (verifyTokenApi && (verifyTokenApi.status === undefined || verifyTokenApi.status !== 200)) {
            if (_fs.existsSync("token.txt")) {
                _fs.unlink("token.txt", function () { });
            }
            // console.log("Token_ERROR_False", err);
            return false;
        } else {
            return token;
        }
    },
    tokenValidation: async (campaign_source = 'default') => {
        let fileName = (campaign_source == 'dormant') ? configConstant.Details.dormantToken : configConstant.Details.token;
        try {
            return await new Promise(async (resolve, reject) => {
                let isExit = false;
                try {
                    if (_fs.existsSync(fileName)) {
                        isExit = true;
                    }
                } catch (err) {
                    isExit = false;
                }

                if (isExit) {
                    let stats = _fs.statSync(`${fileName}`);
                    if (!stats) { return false; }
                    let today = new Date();
                    let filetime = new Date(stats.ctime);
                    let diffMs = (today - filetime);
                    let diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
                    if (diffMins < configConstant.Details.TokenTime) {
                        const rl = readline(fileName, {});
                        rl.on("line", async function (TokenData, linecount) {
                            console.log('TokenData file', TokenData)
                            resolve({ token: TokenData, message: "usable token" });
                        });
                    }
                    else {
                        resolve({ token: "", message: "expired token" });
                    }
                }
                else {
                    resolve({ token: "", message: "token not found" });
                }
            });
        } catch (error) {
            console.log("Optimove_helper_error", error)
            return error;
        }
    },
    tokenGeneration: async (loginUrl, campaign_source = 'default') => {
        let requestParam = {
            'UserName': configConstant.Details._OptimoveDetails._LoginUserName,
            'Password': configConstant.Details._OptimoveDetails._LoginPassword
        };
        if (campaign_source == 'dormant') {
            requestParam = {
                'UserName': configConstant.Details._OptimoveDetails._DormantAccount._LoginUserName,
                'Password': configConstant.Details._OptimoveDetails._DormantAccount._LoginPassword
            };
        }

        let option = {
            method: 'post',
            url: loginUrl,
            data: requestParam,
            headers: { 'content-type': 'application/json' },
            responseType: 'json',
        };
        let Apiresult = await axios(option);
        if (!_.isEmpty(Apiresult.data)) {
            console.log('TokenData login', Apiresult.data)
            let TokenData = Apiresult.data;
            let fileName = (campaign_source == 'dormant') ? configConstant.Details.dormantToken : configConstant.Details.token
            _fs.writeFile(fileName, TokenData, function (err) {/*if (err) throw err ;*/ });
            return TokenData;
        } else {
            console.log("TokenData login_error", Apiresult);
            return "";
        }
    },
    getVerifiedToken: async (campaign_source = 'default') => {
        let validToken = await module.exports.tokenValidation(campaign_source);
        let token = validToken.token;
        let verification;
        if (!validToken || validToken && validToken.token === "" || validToken && validToken.token == null) {
            token = await module.exports.tokenGeneration(configConstant.Details._OptimoveDetails._Login_Api_Url, campaign_source);
        }
        verification = await module.exports.tokenVerification(token).catch((err) => {
            return false;
        });
        if (!verification) {
            let newToken = await module.exports.tokenGeneration(configConstant.Details._OptimoveDetails._Login_Api_Url, campaign_source);
            try {
                await module.exports.tokenVerification(newToken).then(async (resp) => {
                    return newToken;
                }).catch(async (err) => {
                    console.log("Token_ERROR->", err);
                    throw err;
                });
            }
            catch (err) {
                let subject = 'Alert - ZM optimove token failed';
                console.log("Optimove_helper_error", err)
                //return await alertService.handleError(err, subject);
            }
        } else {
            return verification;
        }
    },
    saveRecordsInDynamo : async (record, type) => { 
        let params = {};
        if(type == 'insert'){
            params = { 
                'TableName': configConstant.Details.dynamoTableName,
                "Item": record,
                "ConditionExpression": "attribute_not_exists(#key)",
                "ExpressionAttributeNames": {
                    "#key": "UserId"
                },
                "ReturnValues": "NONE"
            };
            // console.log(params)
            let resp = await dynamoModels.putInDynamo(params);
            return await resp;
        }else{
            
            params = {
                'TableName': configConstant.Details.dynamoTableName,
                ... record
            };
            // console.log(params)
            let resp = await dynamoModels.update(params);
            return await resp;
        }
        
    } 
}


// module.exports.getVerifiedToken('default').then(async(resp) =>{
//     console.log(resp)
// })