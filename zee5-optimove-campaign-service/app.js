const AWS = require('aws-sdk');
const axios = require('axios');
let moment = require('moment');
const dynamoModels = require('./db/dynamoConnect');
const redisConnect = require('./db/redisConnect');
const _helper = require('./helper/helper');
const configConstant = require('./config/config');
AWS.config.update({
    region: 'ap-south-1',
    //endpoint: "http://localhost:8000"
});

const dynamodb = new AWS.DynamoDB();
const dynamo = new AWS.DynamoDB.DocumentClient();


// redisConnect.on("connect", function() {
//     console.log("You are now connected");
// });

// async function redistry(){
//     return await new Promise(async(resolve, reject) =>{
//              redisConnect.getRedisObj('jsree').then(async(resp)=>{
//                 console.log(resp,"-------");
//             resolve(resp)
//             }).catch(err =>{
//                 console.log(err,"-------++++++")
//                 reject(err)
//             })
            
//     })
//     .then((resp) =>{
//         console.log(resp)
//         return resp
//     }).catch(err =>{
//         console.log(err)
//         return err;
//     })
// }
//alternate redis structure
// "In-App":{
//     "userId": val
// }
// "high-low":{
// "userId":val
// }


let insertUsers = async function () {
    let params = {
        TableName: configConstant.Details.dynamoTableName,
        KeySchema: [{
            AttributeName: "UserId",
            KeyType: "HASH"
        }],
        AttributeDefinitions: [{
            AttributeName: "UserId",
            AttributeType: "S"
        }],
        ProvisionedThroughput: {
            ReadCapacityUnits: 1000,
            WriteCapacityUnits: 1000
        }
    };
    return await dynamoModels.createNewTable(params);
};

let getUserData = async function (page) {
    let token = "Hb90lnWMv3xpMwRBA8dirwwNLrf31gDK";
    let AuthToken = await _helper.getVerifiedToken();
    let CampaignID = 129411;
    let Get_Customer_Exrcution_details_Api_Url = `https://api4.optimove.net/current/customers/GetCustomerExecutionDetailsByCampaign`;
    let top = 100;
    let channel = '509';
    // let  urls = `${Get_Customer_Exrcution_details_Api_Url}?CampaignID=${CampaignID}&CustomerAttributes=Email;Country&IncludeControlGroup=True`;
    let urls = `${Get_Customer_Exrcution_details_Api_Url}?CampaignID=${CampaignID}&ChannelID=${channel}&CustomerAttributes=Email;Mobile;FirstName&$top=${top}`;
    if (page > 0) {
        // urls = `${Get_Customer_Exrcution_details_Api_Url}?CampaignID=${CampaignID}&CustomerAttributes=Email;Country&$skip=${page}&IncludeControlGroup=True`;
        urls = `${Get_Customer_Exrcution_details_Api_Url}?CampaignID=${CampaignID}&ChannelID=${channel}&CustomerAttributes=Email;Mobile;FirstName&$top=${top}&$skip=${page}`;
    }
    console.log(urls)
    let option = {
        url: urls,
        method: 'GET',
        headers: {
            'Authorization-Token': AuthToken
        },
        responseType: 'json',
    }
    console.log('helperResponse', option)
    return await new Promise(async (resolve) => {
        let res = await axios(option);
        resolve(res.data);
    })
}

let putDataInDynamo = async function () {
    let userData = await getUserData(10);
    userData.forEach(async (user) => {
            //let item = await getDataFromDynamo(user.CustomerID);
            console.log(user)
            let date = moment().format("YYYY-MM-DD HH:mm:ss");
            let params = {
                    "TableName": configConstant.Details.dynamoTableName,
                    "Item": {
                        'UserId':  user.CustomerID,
                        'CampaignNames': ["In-App"],
                        'Campaigns':[{
                                    "CampaignId": 1,
                                    "CampaignName": "In-APP",
                                    "Impression": 0,
                                        "Engagement": {
                                            "UserCancel":"TRUE",
                                            "InAppRating":4,
                                            "EngagedAt": date
                                        }
                                },],
                    "LastUpdateAt":date,
                    "CreatedAT": date,
                    "Zee5Platform":"Web",
                    "Zee5Version":"2.0"
                },
                "ConditionExpression" :"attribute_not_exists(#key)",
                "ExpressionAttributeNames" : {"#key":"UserId"},
                "ReturnValues": "ALL_OLD",
        };
        let resp  = await dynamoModels.putInDynamo(params);
        if(resp.code != "ConditionalCheckFailedException"){
            console.log("pre-existing userId ", "should check for CampaignName")
        }else{

        }
    })
}


let dropTable = async function () {
    let table = {
        TableName: configConstant.Details.dynamoTableName
    };
    let docClient = new AWS.DynamoDB.DocumentClient();
    console.log(table)
    dynamodb.deleteTable(table, async (err, data) => {
        if (err) {
            console.log(err)
        } else {
            console.log(data)
        }
    });
}

// let queryDocs = async function() {
//     let params ={
//         "TableName" : "Campaign_Users",
//         "KeyConditionExpression": "CampaignNames NOT_CONTAINS(:name)",
//         "KeyConditionExpressionValues":{
//             ":name": "In-App"
//         }
//     };
    
//     dynamo.query(params, function(err, data) {
//         if (err) {
//             console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
//         } else {
//             console.log("Query succeeded.");
//             data.Items.forEach(function(item) {
//                 console.log(" -", item.year + ": " + item.title);
//             });
//         }
//     });
// }

//redistry()
// aws dynamodb query --table-name Campaign_users --endpoint-url http://localhost:8000
//dropTable()
//insertUsers()