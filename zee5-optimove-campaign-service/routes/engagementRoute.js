const router = require('express').Router();
const configConstant = require('../config/config');
const _helper = require('../helper/helper');
const controllers = require('../controllers/controllers');

router.route(`/${configConstant.Details._api_version}/userEngagementEventListener`).post(async (req, res)=>{
	console.log('campaign service ============ ', req.body)
	let ResultResp = [];
	try {
		ResultResp['message'] = 'campaign service Event listner function';
		ResultResp = await controllers.engagementController(req.body);
		let result = await _helper.ResponseAndError(ResultResp);
		res.json(result)
	} catch (err) {
		return err;
	}
})

router.route(`/${configConstant.Details._api_version}/getUserCampaign`).get(async (req, res)=>{
    console.log('campaign service ============ ', req.query)
	let ResultResp = [];
	try {
        ResultResp['message'] = 'campaign service Get User Campaign';
        ResultResp = await controllers.getUserCampaign(req.query.UserId);
		let result = await _helper.ResponseAndError(ResultResp);
		res.json(result)
	} catch (err) {
		return err;
	}
})

router.route(`/${configConstant.Details._api_version}/setUserImpression`).post(async (req, res)=>{
    console.log('campaign service ============ ', req.body)
	let ResultResp = [];
	try {
        ResultResp['message'] = 'campaign service Set User Impression';
        ResultResp = await controllers.setUserImpression(req.body);
		let result = await _helper.ResponseAndError(ResultResp);
		res.json(result)
	} catch (err) {
		return err;
	}
})
module.exports = router;